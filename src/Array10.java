import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int num = 0;
        int arr[];

        int uninum = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        num = sc.nextInt();
        arr = new int[num];

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            int temp = sc.nextInt();
            int index = -1;
            for (int j = 0; j < uninum; j++) {
                if (arr[j] == temp) {
                    index = j;
                }

            }
            if (index < 0) {
                arr[uninum] = temp;
                uninum++;
            }
        }
        System.out.print("arr = ");
        for (int i = 0; i < uninum; i++) {
            System.out.print(arr[i] + " ");
        }
        sc.close();
    }
}
