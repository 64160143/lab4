import java.util.Scanner;

public class MyApp {

    static void printwelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void printmenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static int inputchoice() {
        int choice;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input yout choice(1-3):");
            choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3:");
        }

    
    }

    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printwelcome();
            printmenu();
            choice = inputchoice();
            process(choice);

            
           
        }

    }
    static void process(int choice){
        switch (choice) {
            case 1:
                printhelloworld();
                break;
            case 2:
                addtwonumber();
                break;
            case 3:
                exitprogram();
                break;

        }
    }
    static int add(int fir,int sec){
        int resualt = fir + sec;
        return resualt;

    }
    static void addtwonumber(){
        Scanner sc = new Scanner(System.in);
        int fir,sec;
        int resualt;
        System.out.print("Please input first number: ");
        fir = sc.nextInt();
        System.out.print("Please input Second number: ");
        sec = sc.nextInt();
        resualt = add(fir,sec);
        System.out.println("Result = "+resualt);

    }
    static int inputtime(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        return time;
    }

    static void printhelloworld(){
        int time = inputtime(); 
        for(int i = 0;i<time;i++){
            System.out.println("Hello World!!!");
        }

    }
    static void exitprogram(){
        System.exit(0);
        System.out.println("Bye!!");
    }
}
